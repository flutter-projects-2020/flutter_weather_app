import 'package:flutter/material.dart';
import 'package:weather_app/pages/city_screen.dart';
import 'package:weather_app/pages/loading_screen.dart';
import 'package:weather_app/pages/location_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark().copyWith(
          textTheme: TextTheme(body1: TextStyle(fontFamily: 'Spartan MB'))),
      home: LoadingScreen(),
//      home: CityScreen(),
//      home: LocationScreen(),
    );
  }
}

/**What you will learn
    How to use Dart to perform asynchronous tasks.
    Understand async and await.
    Learn about Futures and how to work with them.
    How to network with the Dart http package.
    What APIs are and how to use them to get data from the internet.
    What JSONs are and how to parse them using the Dart convert package.
    How to pass data forwards and backwards between screens using the Navigator.
    How to handle exceptions in Dart using try/catch/throw.
    Learn about the lifecycle of Stateful Widgets and how to override them.
    How to use the Geolocator package to get live location data for both iOS and Android.
    How to use the TextField Widget to take user input.
 * */
//
//void main() {
//  performTasks();
//}
//
//void performTasks() async {
//  task2(); // here you do task2 in the main event-loop & go beyond | when the event-loop encounters await keyword in the task2 body, it registers what to be done after the function marked await gets completed, & goes away from there to do the next tasks
//  print('perfomrmTasks finished not');
//  await task2(); // here you send task2() to perform in another isolate, & in this isolate you do other tasks in the event loop till the task2 from another isolate gets completed
//  // from here what to be done after task2 completes
//  print('perfomrmTasks finished');
//}
//
////Functions marked 'async' must have a return type assignable to 'Future'.
//Future<void> task2() async {
//  Duration threeSecs = Duration(seconds: 3);
//  String result;
//
//  // currently this future instance is in uncomplete state, & if now we execute this function like a sync one our result will be null
//  Future.delayed(threeSecs, () {
//    result = 'result from task2';
//  });
//  print(result);
//  // so what will be do is, we will wait for this future to be in its complete state, that means we will await, (for completionEvent), and then we will return the result with the value non-null
//  await Future.delayed(threeSecs, () {
//    result = 'result from task2 2';
//  });
//  print(result);
//  // for that we will need to mark this body as async,
//  // then because the body is marked as async, we have change its return type to a Future
//  print('task2 finished');
//}
