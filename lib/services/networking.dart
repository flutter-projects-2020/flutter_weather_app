import 'dart:convert';

/// to make a network request call the http.get(pass_in_the_url_want_to_fetch_data_from)
import 'package:http/http.dart'
    as http; // all the identifiers in package:http/http.dart need to referred by prefixing http

class API {
  final String url; // an API connects you & the dynamic-server(providing Web Service)
  API({this.url});

  Future<dynamic> getJSON() async {
    http.Response r = await http.get(url); // Request a dynamic-server(a server that generates data) | normally returns structured-data in JSON format
    if (r.statusCode == 200) { //  http.Response class contains the data received from a successful http call.
      return jsonDecode(r.body); //jsonDecode takes a json string
    } else
      print(r.statusCode);
  }
}

//double temp = jsonObj['main']['temp'];
//int cond = jsonObj['weather'][0]['id'];
//String cityName = jsonObj['name'];
