import 'package:flutter/material.dart';
import 'package:weather_app/pages/location_screen.dart';
import 'package:weather_app/services/location.dart';
import 'package:weather_app/services/networking.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

const apiKey = 'a5f5070a0ae93a33f6e2554de430d1e2';

class LoadingScreen extends StatefulWidget {
  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  // await allows the program to register a callback/work to be done on the completionEvent while continuing to do other things,
  // then continue to where it left off when the result comes back
  Future<void> getLocationWeatherData() async {
    Location l = Location();
    await l.getCurrentLocation(); // ''async-work(work in another isolate)''
    //before accessing the intended variables, wait for above method to complete, let it complete its job
    //then use the variables
    dynamic jsonWeatherData = await API(
      url:
          'https://api.openweathermap.org/data/2.5/weather?lat=${l.latitude}&lon=${l.longitude}&appid=$apiKey&units=metric',
    ).getJSON();

    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => LocationScreen(jsonWeatherData)));
  }

  @override
  void initState() {
    super.initState();
    // to call a future returning function, you need to prefix the await keyword in order to get the complete-value not uncomplete-value
    //rather than awaiting an ''async-work(work in another isolate)''
    //directly inside initState, call a seperate method to do this work
    // the call to an await containing function is made here, its process is initiated, & this main-EventLoop keeps going-on, // todo: Conform this happens or not ?? -> later when the value comes, & if the value is used in code then, flutter rebuilds, either the use is variable-assignment or in a print
    getLocationWeatherData(); // here I am just initiating a long operation to happen in another isolate rather than in my main-isolate
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              for (var i in ['Fetching', 'your', 'current', 'location', '....'])
                Text(i),
              SpinKitDoubleBounce( // todo: use Circular Progress Indicator
                color: Colors.white,
              )
            ],
          ),
        ),
      ),
    );
  }
}

/** async & await
 * to get the time consuming tasks to happen in the background(another isolate) instead of the foreground(this main-isolate)
 * a way for us to be able to carry out time consuming tasks such as
 *  - getting the G.P.S. location from the phone
 *  - trying to get downloaded from the internet
 *  - trying to read something from a file
 */
