import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:weather_app/utils/SizeConfig.dart';

class CityScreen extends StatefulWidget {
  @override
  _CityScreenState createState() => _CityScreenState();
}

class _CityScreenState extends State<CityScreen> {
  String queryCity;
  @override
  Widget build(BuildContext context) {
    SC().init(context);
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          image: DecorationImage(
              //creates an image to show in BoxDecoration (or here we can say a container)
              image: AssetImage('assets/images/me0.jpg'),
              fit: BoxFit.cover),
        ),
        child: SafeArea(
          child: Column(
            children: <Widget>[
              Align(
                alignment: Alignment.topLeft,
                child: FlatButton(
                  onPressed: () {},
                  child: Icon(
                    Icons.arrow_back,
                    size: 50.0,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(20.0 / 7.37 * SC.hf),
                child: TextField(
                  onChanged: (v) {
                    queryCity = v;
                  },
                  style: TextStyle(color: Colors.black),
                  decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      hintText: 'Enter city name',
                      hintStyle: TextStyle(color: Colors.grey),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide.none),
                      icon: Icon(
                        Icons.location_city,
                        color: Colors.white,
                      )),
                ),
              ),
              FlatButton(
                onPressed: () {
                  Navigator.pop(context, queryCity);
                },
                child: Text(
                  'Get Weather',
                  style: TextStyle(fontSize: 30.0),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
