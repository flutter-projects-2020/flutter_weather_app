import 'package:flutter/widgets.dart';

// the size configuration file
class SC {
  static MediaQueryData _window;

  static double screenHeight;
  double _obscuredHeight;
  static double safeHeight;

  static double screenWidth;
  double _obscuredWidth;
  static double safeWidth;

  static double hf;
  static double wf;

  static Orientation orientation;
  static bool isMobile; // isNotTablet

// function to initialize this class in a build method
  void init(BuildContext context) {
    _window = MediaQuery.of(context);

    screenHeight = _window.size.height;
    _obscuredHeight = _window.padding.top + _window.padding.bottom;
    safeHeight = (screenHeight - _obscuredHeight);

    screenWidth = _window.size.width;
    _obscuredWidth = _window.padding.left + _window.padding.right;
    safeWidth = (screenWidth - _obscuredWidth);

    orientation = MediaQuery.of(context).orientation;
    isMobile = screenWidth < 450 ? true : false;

    hf = (orientation == Orientation.portrait && isMobile
            ? safeHeight
            : safeWidth) /
        100;
    wf = (orientation == Orientation.portrait ? safeWidth : safeHeight) / 100;

    // size of text: If in portrait, respect to screen height | If in landscape respect to screen width
    // size of image: If in portrait, respect to screen width | If in landscape respect to screen height
  }
}
