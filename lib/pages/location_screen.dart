import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:weather_app/pages/city_screen.dart';
import 'package:weather_app/utils/SizeConfig.dart';

class LocationScreen extends StatefulWidget {
  LocationScreen(this.jsonWeatherData);
  final dynamic jsonWeatherData;
  @override
  _LocationScreenState createState() => _LocationScreenState();
}

class _LocationScreenState extends State<LocationScreen> {
  int temp;
  int wId;
  String wDesc;
  String city;

  @override
  void initState() {
    super.initState();
    uI(widget.jsonWeatherData);
  }

  void uI(dynamic d) {
    temp = (d['main']['temp']).toInt();
    wId = d['weather'][0]['id'];
    wDesc = d['weather'][0]['description'];
    city = d['name'];
  }

  @override
  Widget build(BuildContext context) {
    SC().init(context);
    return Scaffold(
      body: Container(
        /// Now the image in the container is outside of SafeArea, painted in the whole screen
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/me1.jpg'),
            fit: BoxFit.cover,
            colorFilter: ColorFilter.mode(
                Colors.white.withOpacity(0.8), BlendMode.dstATop),
          ),
        ),
//        constraints: BoxConstraints.expand(),
        child: SafeArea(
          /// But the child are in the SafeArea
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  FlatButton(
                    onPressed: () async {
                      dynamic queryCity = await Navigator.of(context).push(
                          MaterialPageRoute(
                              builder: (context) => CityScreen()));
                      if (queryCity != null) {
                        // todo: fetch & show queried city weather
                      }
                    },
                    child: Icon(
                      Icons.near_me,
                      size: 50.0 / 3.92 * SC.wf,
                    ),
                  ),
                  FlatButton(
                    onPressed: () {},
                    child: Icon(
                      Icons.location_city,
                      size: 50.0 / 3.92 * SC.wf,
                    ),
                  )
                ],
              ),
              Row(
                children: <Widget>[
                  Text(
                    '$temp°',
                    style: TextStyle(fontSize: 100.0 / 7.37 * SC.hf),
                  ),
                  Expanded(
                    child: Text(
                      "$wDesc in $city",
                      style: TextStyle(fontSize: 60.0 / 7.37 * SC.hf),
                      textAlign: TextAlign.right,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
